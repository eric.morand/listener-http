# HTTP listener for [Arabesque](https://www.npmjs.com/package/@arabesque/core)

[![NPM version][npm-image]][npm-url] [![Coverage Status][coverage-image]][coverage-url]

## Getting started

HTTP listener for Arabesque consists of a context type and a factory that returns an Arabesque Listener.

### Context

```ts
import type {IncomingMessage, ServerResponse} from "http";

declare type Context<Message extends IncomingMessage, Response extends ServerResponse> = {
    message: Message;
    response: Response;
};
```

### createListener

```ts
import type {Server, IncomingMessage, ServerResponse} from "http";

declare const createListener: <Message extends IncomingMessage, Response extends ServerResponse>(client: Server) => Listener<number, Context<Message, Response>>;
```

The factory that creates a Kafka listener. The returned listener is guaranteed to close the passed client when the
application is stopped.

## Usage

### HTTP

```ts
import {createApplication} from "@arabesque/core";
import {createListener} from "@arabesque/listener-http";
import {createServer} from "http";

const listener = createListener(createServer());

const application = createApplication(listener);

application.listen(3000);
```

### HTTPS

```ts
import {createApplication} from "@arabesque/core";
import {createListener} from "@arabesque/listener-http";
import {createServer} from "https";
import {readFileSync} from "fs";

const listener = createListener(createServer({
    key: readFileSync('key.pem'),
    cert: readFileSync('cert.pem'),
    passphrase: 'passphrase'
}));

const application = createApplication(listener);

application.listen(3000);
```

#### Using a custom response

```ts
import {createApplication} from "@arabesque/core";
import {createListener} from "@arabesque/listener-http";
import {createServer, IncomingMessage, ServerResponse} from "http";

class CustomResponse extends ServerResponse {
    public body: string = '';

    end(cb: () => void) {
        super.end(this.body, cb);
    }
}

const listener = createListener<IncomingMessage, CustomResponse>(createServer({
    ServerResponse: CustomResponse
}));

const application = createApplication(listener);

application.use((context, next) => {
    context.response.body = 'foo';

    return next(context);
});

application.listen(3000);
```

## Contributing

* Fork this repository
* Code
* Implement tests using [tape](https://github.com/substack/tape)
* Issue a merge request keeping in mind that all pull requests must reference an issue in the issue queue

[npm-image]: https://badge.fury.io/js/@arabesque%2Flistener-http.svg

[npm-url]: https://www.npmjs.com/package/@arabesque/listener-http

[coverage-image]: https://coveralls.io/repos/gitlab/arabesque/listener-http/badge.svg

[coverage-url]: https://coveralls.io/gitlab/arabesque/listener-http