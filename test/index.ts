import * as tape from "tape";
import {IncomingMessage, ServerResponse, RequestListener, Server} from "http";
import {createListener} from "../src";
import {spy, stub} from "sinon";
import {Socket} from "net";

tape('listener', ({same, end}) => {
    let passedOnRequestHandler: RequestListener;

    const client: {
        on: Server["on"];
        listen: Server["listen"];
        close: Server["close"];
    } = {
        on: (eventName, handler) => {
            if (eventName === "request") {
                passedOnRequestHandler = handler as RequestListener;
            } else if (eventName === "listening") {
                (handler as () => void)();
            }

            return this as any;
        },
        listen: () => {
            return this as any;
        },
        close: (callback) => {
            callback!();

            return this as any;
        }
    };

    const spiedClientClose = spy(client, "close");

    const spiedHandler = spy((context) => {
        return Promise.resolve(context);
    });

    const listener = createListener(client as Server);

    return listener(1234, spiedHandler).then((stop) => {
        const message = new IncomingMessage({} as Socket)
        const response = new ServerResponse(message);

        stub(response, "end").callsFake((callback) => {
            callback();
        });

        // simulate a request received by the client
        passedOnRequestHandler(message, response);

        same(spiedHandler.called, true, 'should call the handler when a message is received');

        return stop().then(() => {
            same(spiedClientClose.called, true, 'should close the client when stopped');
        });
    }).finally(() => {
        end();
    });
});