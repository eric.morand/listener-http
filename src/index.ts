import type {Listener} from "@arabesque/core";
import type {Server} from "http";
import type {IncomingMessage, ServerResponse} from "http";

export type Context<Message extends IncomingMessage, Response extends ServerResponse> = {
    message: Message;
    response: Response;
};

export const createListener = <Message extends IncomingMessage, Response extends ServerResponse>(
    client: Server
): Listener<number, Context<Message, Response>> => {
    return (channel, handle) => {
        return new Promise((resolve) => {
            client.on("request", (message: Message, response: Response) => {
                const context = {message, response};

                return handle(context).then((context) => {
                    return new Promise((resolve) => {
                        const {response} = context;

                        response.end(() => resolve(undefined));
                    });
                });
            });

            client.listen(channel);

            client.on("listening", () => {
                const stop = (): Promise<void> => {
                    return new Promise((resolve) => {
                        client.close(() => {
                            resolve();
                        })
                    });
                };

                resolve(stop);
            });
        });
    };
}